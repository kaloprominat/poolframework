#!/usr/bin/env python
# -*- coding: utf-8 -*-

from threading import Condition, Semaphore


class Object(object):

    def __init__(self):
        self._lock = Semaphore()
        self._locked = False

    # properties
    def isLocked(self):
        return self._locked

    # methods
    def lock(self):
        self._locked = True
        return self._lock.acquire()

    def release(self):
        if self._locked is True:
            self._locked = False
            self._lock.release()

    def test(self):
        return True

    def recreate(self):
        pass


class Pool(object):

    """docstring for ObjectsPool"""

    # maxObjects = 10

    def __init__(self, *args, **kwargs):

        super(Pool, self).__init__()

        self.__dict__ = {}

        self.obj_class = kwargs.get('object_class')
        self.obj_kwargs = kwargs.get('object_kwargs')
        self.max_objects = kwargs.get('max_objects', None)

        self._args = args
        self._kwargs = kwargs

        # For 1st instantiation lets setup all our variables
        if 'lock' not in self.__dict__:
            self.lock = Condition()

        if 'objects' not in self.__dict__:
            self.objects = []

    def _getObjectFromSet(self):
        obj = None

        for ob in self.objects:
            if not ob.isLocked():

                ob.lock()

                if ob.test() is False:
                    ob.recreate()

                ob.release()
                obj = ob

        return obj

    def _createObject(self):
        obj = self.obj_class(**self.obj_kwargs)
        return obj

    def getObject(self):

        obj = None

        self.lock.acquire()

        obj = self._getObjectFromSet()

        if obj is None:

            try:
                obj = self._createObject()
            except Exception, e:
                self.lock.release()
                raise e
            else:
                self.objects.append(obj)

        obj.lock()
        self.lock.release()

        return obj

    def returnObject(self, obj):
        obj.release()
