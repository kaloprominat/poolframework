#!/usr/bin/python
# -*- coding: utf-8 -*-

import pool
import pymysql
import datetime


class MysqlConnection(pool.Object):

    """docstring for MysqlConnection"""

    def __init__(self, *args, **kwargs):

        super(MysqlConnection, self).__init__()

        self.kwargs = kwargs
        self.connection = None
        self.lastConnectionCheck = None

        self._connect()

    def test(self):
        if self.connection is None:
            return False

        try:
            cursor = self.connection.cursor(pymysql.cursors.DictCursor)
            cursor.execute('select current_user')
            self._updateCheckTime()
            return True
        except Exception, e:
            self.connection.close()
            self.connection = None
            return False

    def recreate(self):
        self._close()
        self._connect()

    def _close(self):
        if self.connection is not None:
            try:
                self.connection.commit()
                self.connection.close()
                self.connection = None
            except Exception, e:
                pass

    def _connect(self):
        self.connection = pymysql.connect(*[], **self.kwargs)
        self._updateCheckTime()

    def _updateCheckTime(self):
        """
        Updates the connection check timestamp
        """
        self.lastConnectionCheck = datetime.datetime.now()

    def getCursor(self):
        return self.connection.cursor(pymysql.cursors.DictCursor)

    def commit(self):
        return self.connection.commit()


class dbException(Exception):
    def __init__(self):
        super(dbException, self).__init__()
        self.result = None


class dbDuplicateKeyException(dbException):
    def __init__(self, errcode, errmsg, query):
        super(dbDuplicateKeyException, self).__init__()
        self.errcode = errcode
        self.errmsg = errmsg
        self.query = query


class db:

    def __init__(self, **kwargs):

        self.ConnectionPool = pool.Pool(object_class=MysqlConnection, object_kwargs=kwargs, max_objects=10)

    def getConnection(self):
        return self.ConnectionPool.getObject()

    def returnConnection(self, connection):
        return self.ConnectionPool.returnObject(connection)

    def EscapeString(self, text):
        escapeConnection = self.getConnection()
        escapedString = escapeConnection.connection.escape(text).rstrip("'").lstrip("'")
        self.returnConnection(escapeConnection)
        return escapedString

    def query(self, query):
        connection = self.getConnection()
        cursor = connection.getCursor()

        rresult = {
            'status'    : False,
            'count'     : None,
            'result'    : None,
            'errcode'   : None,
            'errmsg'    : None,
            'lastrowid' : None,
            'query'     : None
        }

        try:
            cursor.execute(query)

        except pymysql.MySQLError as e:

            connection.commit()
            self.returnConnection(connection)

            rresult.update({'count' : cursor.rowcount, 'errcode' : e.args[0], 'errmsg' : e.args[1], 'query' : query})

            if e.args[0] == 1062:
                ex = dbDuplicateKeyException(e.args[0], e.args[1], query)
                ex.result = rresult
                raise ex
            else:
                raise e

            return rresult

        except:
            connection.commit()
            self.returnConnection(connection)
            raise

        finally:
            connection.commit()
            self.returnConnection(connection)

        if cursor.rowcount > 0:
            rresult.update({ 'status' : True, 'count' : cursor.rowcount, 'result': cursor.fetchall(),
                           'query' : query, 'lastrowid' : cursor.lastrowid })
        else:
            rresult.update({ 'status' : True, 'count' : 0, 'result' : [] , 'query' : query, 'lastrowid' : cursor.lastrowid })

        return rresult

    def getArray(self, data, table, more=""):
        querystring = 'SELECT {data} FROM {table} {more}'.format(data=data, table=table, more=more)
        return self.query(querystring)

    def getMysqlVersion(self):
        return self.query('SELECT VERSION()')

    def delete(self, table, more="", what=""):
        querystring = 'DELETE {what} FROM {table} WHERE {more}'.format(table=table, more=more, what=what)
        return self.query(querystring)

    def update(self, table, setdata, more=""):
        querystring = 'UPDATE {table} SET {setdata} WHERE {more}'.format(table=table, setdata=setdata, more=more)
        return self.query(querystring)

    def insert(self, table, setdata, more=""):
        querystring = 'INSERT INTO %s %s %s' % (table, setdata, more)
        return self.query(querystring)
